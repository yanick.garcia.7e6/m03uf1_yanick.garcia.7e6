﻿using System;

namespace _2._1DotLine
    /*CREADOR: Yanick
     *DATA:11/10/22
     *DESCRIPCIÓ: Primer exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "for" perque la consola torni un nombre de punts equivalent al número enter que has escrit.
     **/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un número");
            int i = Convert.ToInt32(Console.ReadLine());
            for (int j = 0; j < i; j++)
            {
                Console.WriteLine(".");
            }
        }
    }
}
