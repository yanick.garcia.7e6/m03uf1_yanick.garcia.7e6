﻿using System;

namespace _1._3Wait4Five
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Tercer exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "while" perque la consola demani numeros fins que escriguin el 5
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriba un número entero");
            int j = Convert.ToInt32(Console.ReadLine());
            while (j != 5)
            {
                Console.WriteLine("Intentalo de nuevo"); 
                j = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("5 encontrado");
        }
    }
}
