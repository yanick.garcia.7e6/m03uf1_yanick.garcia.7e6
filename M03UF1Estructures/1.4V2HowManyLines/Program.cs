﻿using System;

namespace _1._4V2HowManyLines
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Quart exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "do while" perque la consola compti les lineas que has introduit
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriba su texto linea a linea");
            string texto = Console.ReadLine();
            int lineas = 0;
            do
            {
                texto = Console.ReadLine();
                lineas++;
            } while (texto != "END");
                Console.WriteLine("Hay un total de " + lineas + " lineas sin contar END");
        }
    }
}
