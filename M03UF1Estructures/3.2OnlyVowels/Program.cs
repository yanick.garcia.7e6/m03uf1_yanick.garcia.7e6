﻿using System;

namespace _3._2OnlyVowels_
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Segon exercici de l'apartat 3 d'exercicis del sites perteneixent a estructures de repetició, s'ha d'introduir un número, despres un conjunt de lletres separades per espais igual al primer número i la consola haurà de retornar les que són vocals
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce el numero de letras que escribiras");
            int numLletres = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ahora escribe las letras separadas por espacios");
            for (int i = 0; i < numLletres * 2; i++)
            {
                int lletra = Console.Read();
                if (lletra == 97 | lletra == 101 | lletra == 105 | lletra == 111 | lletra == 117)
                {
                    char vocal = Convert.ToChar(lletra);
                    Console.Write(vocal + " ");
                }
            }
        }
    }
}
