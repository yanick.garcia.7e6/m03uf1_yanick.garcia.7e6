﻿using System;

namespace _3._4ManualPow
/*CREADOR: Yanick
*DATA:19/10/22
*DESCRIPCIÓ: Quart exercici de l'apartat 3 d'exercicis del sites perteneixent a estructures de repetició, s'ha d'introduir dos enters (base i exponent) imprimeix el resultat de la potencia.
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce la base de la operacion");
            int baseop = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce el exponente de la operacion");
            int expop = Convert.ToInt32(Console.ReadLine());
            int i = 1;
            int baseop2 = baseop;
            while (i != expop)
            {
                baseop2 = (baseop2 * baseop);
                    i++;
            }
            Console.WriteLine(baseop2);
        }
    }
}
