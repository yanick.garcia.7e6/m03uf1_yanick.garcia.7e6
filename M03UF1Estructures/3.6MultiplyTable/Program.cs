﻿using System;

namespace _3._6MultiplyTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tablas de multiplicar:");
            int i = 1;
            while (i <= 9)
            {
                for (int j = 1; j <= 10; j++){
                    Console.Write(i*j + " ");
                }
                Console.WriteLine();
                i++;
            }
        }
    }
}
