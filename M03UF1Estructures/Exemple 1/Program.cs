﻿using System;

namespace Exemple_1
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Primer exercici de estructures de repetició, en aquest cas s'ha de fer us del "while" perque la consola escrigui tots els números fins arribar al 10000
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            while (i <= 100000)
            {
                Console.WriteLine("i = {0}", i);
                i++;
            }
        }
    }
}
