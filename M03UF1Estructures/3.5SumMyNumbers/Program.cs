﻿using System;

namespace _3._5SumMyNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce el numero del que quieras ver las cifras sumadas");
            int numbas = Convert.ToInt32(Console.ReadLine());
            int num = 0;
            while (numbas != 0)
            {
                int residu = numbas % 10;
                num += residu;
                numbas /= 10;
            }
            Console.WriteLine("la suma de estos valores es " + num);
        }
    }
}
