using System;

namespace _3._7Pyramid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Por favor introduzca la altura del triangulo ")
            int altura = Convert.ToInt32(Console.ReadLine());
            for (int i=0; i < altura; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Console.Write("#");
                }
                Console.WriteLine();
            }
        }
    }
}