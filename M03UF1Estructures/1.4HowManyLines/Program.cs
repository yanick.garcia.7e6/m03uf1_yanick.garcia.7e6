﻿using System;

namespace _1._4HowManyLines
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Quart exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "while" perque la consola compti les lineas que has introduit
**/
{
    class HowManyLines
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriba un número entero");
            string texto = Console.ReadLine();
            int lineas = 0;
            while (texto != "END")
            {
                texto = Console.ReadLine();
                lineas++;
            }
            Console.WriteLine("Hay un total de " + lineas + " lineas sin contar END");
        }
    }
}
