﻿using System;

namespace _3._1CountWithJumps
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Primer exercici de l'apartat 3 d'exercicis del sites perteneixent a estructures de repetició, s'han d'introduir dos nombres i la consola prendrà el primer com el valor fins al que arribar i el segon com el salt que farà entre números per arribar al primer.
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce el número al que quieres llegar");
            int i = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce el valor de salto (debe ser menor que el primer número)");
            int j = Convert.ToInt32(Console.ReadLine());
            for (int g = 1; g <= i; g+=j)
            {
                Console.WriteLine(g);
            }
        }
    }
}
