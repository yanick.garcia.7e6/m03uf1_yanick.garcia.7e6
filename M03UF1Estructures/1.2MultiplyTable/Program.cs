﻿using System;

namespace _1._2MultiplyTable
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Segon exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "while" perque la consola escrigui la taula de multiplicar del numero corresponent
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce el numero del que quieras saber la tabla");
            int j = Convert.ToInt32(Console.ReadLine());
            int i = 1;
            while (i <= 10)
            {
                Console.WriteLine(i + " multiplicado por " + j + " es igual a " + (i * j));
                i++;
            }
        }
    }
}
