﻿using System;

namespace _1._1Letscount
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Primer exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "while" perque la consola escrigui tots els números fins arribar al que se li ha donat
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un número");
            int j = Convert.ToInt32(Console.ReadLine());
            int i = 0;
            while (i <= j)
            {
                Console.WriteLine("i = {0}", i);
                i++;
            }
        }
    }
}
