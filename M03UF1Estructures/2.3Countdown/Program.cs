﻿using System;

namespace _2._3Countdown
/*CREADOR: Yanick
*DATA:11/10/22
*DESCRIPCIÓ: Tercer exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "for" perque la consola compti enrere desde el número que se li dona.
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un número entero");
            int i = Convert.ToInt32(Console.ReadLine());
            for (int j = i; j> 0; j -= 1)
            {
                Console.WriteLine(j);
            }
        }
    }
}
