﻿using System;

namespace _1._5NumberBetweenOneAndFive
/*CREADOR: Yanick
*DATA:6/10/22
*DESCRIPCIÓ: Cinque exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "while" perque la consola demani que introdueixin un numero fins trobar un entre l'1 i el 5
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un número entre 1 y 5");
            int j = Convert.ToInt32(Console.ReadLine());
            while (j > 5 || j < 1 )
            {
                Console.WriteLine("Este número no sirve, introduce otro");
                j = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("El numero introducido: " + j);
        }
    }
}
