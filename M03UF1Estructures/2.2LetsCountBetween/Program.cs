﻿using System;

namespace _2._2LetsCountBetween
/*CREADOR: Yanick
 *DATA:11/10/22
 *DESCRIPCIÓ: Segon exercici del sites perteneixent a estructures de repetició, en aquest cas s'ha de fer us del "for" perque la consola mostri els numeros que es troben entre els valors donats.
 **/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un número entero");
            int i = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce otro número que sea mayor");
            int j = Convert.ToInt32(Console.ReadLine());
            for (int g = i ; g<j; g++)
            {
                Console.WriteLine(g);
            }
        }
    }
}
