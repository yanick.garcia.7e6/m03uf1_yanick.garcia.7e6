﻿using System;

namespace _3._3GoTournamentGreatestScore
/*CREADOR: Yanick
*DATA:18/10/22
*DESCRIPCIÓ: Tercer exercici de l'apartat 3 d'exercicis del sites perteneixent a estructures de repetició, s'ha d'introduir un nom i una puntuació, un cop s'hagin registrat tots els participants la consola ha de mostrar al guañador i la seva puntuació
**/
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Por favor introduzca su nombre y despues la puntuacion");
            string participante1 = Console.ReadLine();
            int puntuacion1 = Convert.ToInt32(Console.ReadLine());
            string participante2 = Console.ReadLine();
            int puntuacion2 = Convert.ToInt32(Console.ReadLine());
            while (participante2 != "END")
            {
                if (puntuacion2 > puntuacion1)
                {
                    puntuacion1 = puntuacion2;
                    participante1 = participante2;
                }
                participante2 = Console.ReadLine();
                if (participante2 != "END")
                {
                    puntuacion2 = Convert.ToInt32(Console.ReadLine());
                }
            }Console.WriteLine($"El ganador es {participante1} con un total de {puntuacion1}");
        }
    }
}
